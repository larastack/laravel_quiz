@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-sm-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    @if ($message = \Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Laravel Products</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-offset-8 col-sm-3">
                                    <div class="form-group">
                                        <a href="#" class="btn btn-block btn-success" data-toggle="modal" data-target="#myModal">Create Product</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <!-- The Modal -->
                            <div class="modal fade " id="myModal">
                                <div class="modal-dialog modal-lg  modal-dialog-centered">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Create Product</h4>
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <form action="{{ route('product.store') }}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="">Name</label>
                                                    <input type="text" class="form-control" name="name" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Quantity</label>
                                                    <input type="text" class="form-control" name="quantity" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Price</label>
                                                    <input type="text" class="form-control" name="price" />
                                                </div>

                                                <div class="form-group">
                                                    <input type="submit" value="Add Product" class="btn btn-outline-primary">
                                                </div>
                                            </form>
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <!-- The Modal -->
                            <div class="modal fade " id="myModaleidt">
                                <div class="modal-dialog modal-lg  modal-dialog-centered">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Update Product</h4>
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <form id="product_form" action=""
                                                  method="post">
                                                @csrf
                                                {{ method_field('PATCH') }}

                                                <div class="form-group">
                                                    <label for="">Name</label>
                                                    <input type="text" class="form-control" name="name" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Quantity</label>
                                                    <input type="text" class="form-control" name="quantity" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Price</label>
                                                    <input type="text" class="form-control" name="price" />
                                                </div>

                                                <div class="form-group">
                                                    <input type="submit" value="Update Product" class="btn
                                                    btn-outline-primary">
                                                </div>
                                            </form>
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive m-t-40">
                                <table class="display nowrap table table-hover table-striped table-bordered" id="product_table"
                                       cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($data))
                                            @foreach($data as $value)
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->quantity }}</td>
                                                <td>{{ $value->price }}</td>
                                                <td>{{ $value->total_value }}</td>
                                                <td>
                                                    <a data-edit-product="{{ $value->id }}" class="btn
                                                    btn-outline-info edit_product"> Edit</a>

                                                </td>
                                            @endforeach

                                            @else
                                                <td colspan="6">No Data Available</td>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#product_table').DataTable();

            $(document).on('click', '.edit_product', function(ev) {
                ev.preventDefault();
                var val = $(this).data('edit-product');
                $.ajax({
                    url: 'product/'+val,
                    type: 'GET',
                    beforeSend: function ()
                    {
                    },
                    success: function(response) {
                        console.log(response);
                        $('#product_form')
                            .find('[name="name"]').val(response.data.name).end()
                            .find('[name="quantity"]').val(response.data.quantity).end()
                            .find('[name="price"]').val(response.data.price).end();

                        $("#product_form").attr("action", "product/"+response.data.id);
                        $("#myModaleidt").modal({backdrop: 'static', keyboard: true});
                    },
                    error: function(response) {
                        console.log(response);
                        alert('Operation failed');
                    }
                });
            });
        });
    </script>
@endsection