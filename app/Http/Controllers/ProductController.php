<?php


namespace App\Http\Controllers;



use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController
{
    public function index()
    {
        $data = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];
        return view('product', compact('data'));
    }

    public function store(ProductRequest $request)
    {
        // Validate the request before saving it
        $request->validated();
        $contactInfo = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];
        $inputData = $request->only(['name', 'quantity', 'price']);
        $inputData['id'] = Str::random(5);
        $inputData['date_created'] = date('Y-m-d H:i:s');
        $inputData['total_value'] = $request->quantity * $request->price;

        array_push($contactInfo,$inputData);

        Storage::disk('local')->put('data.json', json_encode($contactInfo));
        return back()->with('success','Product created successfully!');
    }

    public function show($id)
    {
        $data = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];
        if (!empty($data)){
            // Convert data to a collection
            $data = collect($data);
            $data->where('id', $id)->first();

            return response()->json([
                'data' => $data[0],
                'message' => 'success',
                'status' => true
            ], 200);
        }

        return response()->json([
            'data' => null,
            'message' => 'failed',
            'status' => false
        ], 404);
    }

    public function update(ProductRequest $request, $id)
    {
        // Validate the request before saving it
        $request->validated();
        $request->id = $id;
        $contactInfo = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];
        $inputData = $request->only(['name', 'quantity', 'price', 'id']);
        $inputData['total_value'] = $request->quantity * $request->price;

        if ($contactInfo){
            $contactInfo = collect($contactInfo);
            $filtered = $contactInfo->filter(function ($value, $key) use ($request){
                return $value->id == $request->id;
            });

            $filtered[0]->name = $request->name;
            $filtered[0]->quantity = $request->quantity;
            $filtered[0]->price = $request->price;
            $filtered[0]->total_value = $request->quantity * $request->price;
        }

        return back()->with('success','Product updated successfully!');
    }
}